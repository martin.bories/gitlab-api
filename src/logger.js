import log from '@megatherium/log';

const logger = log.init({
	app: '@megatherium/gitlab-api',
});

export default logger;
