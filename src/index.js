import createProject from './lib/createProject';
import createProjectMember from './lib/createProjectMember';
import createProjectPipelineSchedule from './lib/createProjectPipelineSchedule';
import createProjectVariable from './lib/createProjectVariable';
import createUserKey from './lib/createUserKey';
import getProject from './lib/getProject';
import GitLabError from './lib/GitLabError';
import listProjects from './lib/listProjects';
import listProjectMembers from './lib/listProjectMembers';
import listProjectPipelineSchedules from './lib/listProjectPipelineSchedules';
import listProjectVariables from './lib/listProjectVariables';
import listUserKeys from './lib/listUserKeys';
import listUsers from './lib/listUsers';
import parseProjectIdOrName from './lib/parseProjectIdOrName';
import removeProject from './lib/removeProject';
import removeProjectMember from './lib/removeProjectMember';
import removeProjectPipelineSchedule from './lib/removeProjectPipelineSchedule';
import removeProjectVariable from './lib/removeProjectVariable';
import removeUserKey from './lib/removeUserKey';
import request from './lib/request';
import updateProjectMember from './lib/updateProjectMember';
import updateProjectPipelineSchedule from './lib/updateProjectPipelineSchedule';
import updateProjectVariable from './lib/updateProjectVariable';

const gitlab = {
	/**
	 * Creates a new project.
	 * @access public
	 * @augments request
	 * @author Martin Bories <martin.bories@megatherium.solutions>
	 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
	 * @example <caption>Create a sample project</caption>
	 * import assert from 'assert';
	 * import {
	 * 	createProject,
	 * 	GitLabError,
	 * } from '@megatherium/gitlab-api';
	 *
	 * (async() => {
	 * 	try {
	 * 		await createProject('sample');
	 * 	} catch (err) {
	 * 		assert.strictEqual(err instanceof GitLabError, true);
	 * 		if (err.error.name !== 'has already been taken'
	 * 			&& err.error.path !== 'has already been taken'
	 * 		) {
	 * 			throw err;
	 * 		}
	 * 	}
	 * })();
	 * @license Megatherium Standard License 1.0
	 * @param {string} pathOrName The name or the path of the project to create.
	 * @param {object=} additionalData Additional data to add to the project. See {@link https://docs.gitlab.com/ee/api/projects.html#create-project|GitLab API}.
	 * @returns {Promise} Resolves after the project has been created.
	 * @see {@link https://docs.gitlab.com/ee/api/projects.html#create-project|GitLab API: POST /projects}
	 * @throws {module:GitLabError} With `{name: 'has already been taken'}` if the name is already used by another project.
	 * @throws {module:GitLabError} With `{path: 'has already been taken'}` if the name is already used by another project.
	 */
	createProject,

	/**
	 * Adds a member to a project.
	 * @access public
	 * @augments request
	 * @author Martin Bories <martin.bories@megatherium.solutions>
	 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
	 * @example <caption>Add a user to the sample-project</caption>
	 * import assert from 'assert';
	 * import {
	 * 	createProjectMember,
	 * 	GitLabError,
	 * } from '@megatherium/gitlab-api';
	 *
	 * (async() => {
	 * 	try {
	 * 		await createProjectMember('sample', 1, 0);
	 * 	} catch (err) {
	 * 		assert.strictEqual(err instanceof GitLabError, true);
	 * 		assert.strictEqual(err.message, '404 Project Not Found');
	 * 	}
	 * })();
	 * @license Megatherium Standard License 1.0
	 * @param {int|string} projectIdOrName The ID or the name of the project.
	 * @param {int} userId The ID of the user.
	 * @param {int} accessLevel The access level the user will be granted.
	 *
	 * - 0: No access
	 * - 10: Guest
	 * - 20: Reporter
	 * - 30: Developer
	 * - 40: Maintainer
	 * @param {object=} additionalData Additional data sent to {@link https://docs.gitlab.com/ee/api/members.html#add-a-member-to-a-group-or-project|POST /projects/:projectIdOrName/members}.
	 * @returns {Promise<object>} Returns the data of the user after he was added as a member to the project.
	 * @see {@link https://docs.gitlab.com/ee/api/members.html#add-a-member-to-a-group-or-project|GitLab API: POST /projects/:projectIdOrName/members}
	 * @throws {module:GitLabError} With "404 Project Not Found" if the project could not be found.
	 */
	createProjectMember,

	/**
	 * Creates a new pipeline schedule for a project.
	 * @access public
	 * @augments request
	 * @author Martin Bories <martin.bories@megatherium.solutions>
	 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
	 * @example <caption>Create an hourly schedule</caption>
	 * import assert from 'assert';
	 * import {
	 * 	createProjectPipelineSchedule,
	 * 	GitLabError,
	 * } from '@megatherium/gitlab-api';
	 *
	 * (async() => {
	 * 	try {
	 * 		await createProjectPipelineSchedule('sample', 'Run build hourly from 8 AM to 10 PM.', 'master', '0 8-22 * *', {
	 * 			cron_timezone: 'UTC+1',
	 * 		});
	 * 	} catch (err) {
	 * 		assert.strictEqual(err instanceof GitLabError, true);
	 * 		assert.strictEqual(err.message, '404 Project Not Found');
	 * 	}
	 * })();
	 * @example <caption>Create an hourly schedule in default timezone</caption>
	 * import assert from 'assert';
	 * import {
	 * 	createProjectPipelineSchedule,
	 * 	GitLabError,
	 * } from '@megatherium/gitlab-api';
	 *
	 * (async() => {
	 * 	try {
	 * 		await createProjectPipelineSchedule('sample', 'Run build hourly from 8 AM to 10 PM.', 'master', '0 8-22 * *');
	 * 	} catch (err) {
	 * 		assert.strictEqual(err instanceof GitLabError, true);
	 * 		assert.strictEqual(err.message, '404 Project Not Found');
	 * 	}
	 * })();
	 * @license Megatherium Standard License 1.0
	 * @param {int|string} projectIdOrName The ID of the project or it's name.
	 * @param {string} description The description of the pipeline schedule.
	 * @param {string} ref The reference to the branch to execute on, e.g. "master".
	 * @param {string} cron The cronjob configuration. See: {@link https://en.wikipedia.org/wiki/Cron|Wikipedia: Cron Syntax}
	 * @param {object=} additionalData Additional data to add to the project. See {@link https://docs.gitlab.com/ee/api/pipeline_schedules.html#create-a-new-pipeline-schedule|POST /projects/:projectIdOrName/pipeline_schedules}.
	 * @returns {Promise} Resolves after the pipeline schedule for the project has been created.
	 * @see {@link https://docs.gitlab.com/ee/api/pipeline_schedules.html#create-a-new-pipeline-schedule|GitLab API: POST /projects/:projectIdOrName/pipeline_schedules}
	 * @see {@link https://en.wikipedia.org/wiki/Cron|Wikipedia: Cron Syntax}
	 * @throws {module:GitLabError} With "404 Project Not Found" if the project could not be found.
	 */
	createProjectPipelineSchedule,

	/**
	 * Creates a new variable for a GitLab project.
	 * @access public
	 * @augments request
	 * @author Martin Bories <martin.bories@megatherium.solutions>
	 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
	 * @example <caption>Create a variable for MEGATHERIUM_NAME</caption>
	 * //global process
	 * import assert from 'assert';
	 * import {
	 * 	createProjectVariable,
	 * 	GitLabError,
	 * } from '@megatherium/gitlab-api';
	 *
	 * (async() => {
	 * 	try {
	 * 		await createProjectVariable('gitlab-api', {
	 * 			name: 'MEGATHERIUM_NAME',
	 * 			value: process.env.MEGATHERIUM_NAME,
	 * 		});
	 * 	} catch (err) {
	 * 		assert.strictEqual(err instanceof GitLabError, true);
	 * 		assert.strictEqual(err.message, '404 Project Not Found');
	 * 	}
	 * })();
	 * @license Megatherium Standard License 1.0
	 * @param {int|string} projectIdOrName The ID of the project or the name of the project.
	 * @param {string} variableName The name of the variable.
	 * @param {string} variableValue The value of the variable.
	 * @param {object=} additionalData Additional data stored for the variable. See {@link https://docs.gitlab.com/ee/api/project_level_variables.html#create-variable|GitLab API}
	 * @returns {Promise} Resolves after the variable has been created.
	 * @see {@link https://docs.gitlab.com/ee/api/project_level_variables.html#create-variable|GitLab API: POST /projects/:projectIdOrName/variables}
	 * @throws {module:GitLabError} With "404 Project Not Found" if the project could not be found.
	 */
	createProjectVariable,

	/**
	 * Adds a new ssh key to the current user.
	 * @access public
	 * @augments request
	 * @author Martin Bories <martin.bories@megatherium.solutions>
	 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
	 * @example <caption>Create the current ssh key</caption>
	 * import assert from 'assert';
	 * import {
	 * 	createUserKey,
	 * 	GitLabError,
	 * } from '@megatherium/gitlab-api';
	 *
	 * const publicKey = `ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAAAgQCqGKukO1De7zhZj6+H0qtjTkVxwTCpvKe4eCZ0FPqri0cb2JZfXJ/DgYSF6vUpwmJG8wVQZKjeGcjDOL5UlsuusFncCzWBQ7RKNUSesmQRMSGkVb1/3j+skZ6UtW+5u09lHNsj6tQ51s1SPrCBkedbNf0Tp0GbMJDyR4e9T04ZZw== example`;
	 *
	 * (async() => {
	 * 	try {
	 * 		await createUserKey('User SSH Key', publicKey);
	 * 	} catch (err) {
	 * 		assert.strictEqual(err instanceof GitLabError, true);
	 * 		assert.strictEqual(err.message, 'has already been taken');
	 * 	}
	 * })();
	 * @license Megatherium Standard License 1.0
	 * @param {string} label The title of the new key.
	 * @param {string} publicKey The public key to store.
	 * @param {object=} additionalData Additional data to store along with the key. See {@link https://docs.gitlab.com/ee/api/users.html#add-ssh-key|GitLab API}.
	 * @returns {Promise} Resolves after the key has been created.
	 * @see {@link https://docs.gitlab.com/ee/api/users.html#add-ssh-key|GitLab API: POST /user/keys}
	 * @throws {module:GitLabError} With `{fingerprint: 'has already been taken'}` when the fingerprint of the public key has already been added to the current user.
	 */
	createUserKey,

	/**
	 * Retrieves the information about a project.
	 * @access public
	 * @augments request
	 * @author Martin Bories <martin.bories@megatherium.solutions>
	 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
	 * @example <caption>Get the data from the sample project</caption>
	 * import assert from 'assert';
	 * import {
	 * 	getProject,
	 * 	GitLabError,
	 * } from '@megatherium/gitlab-api';
	 *
	 * (async() => {
	 * 	try {
	 * 		const project = await getProject('sample', {
	 * 			statistics: true,
	 * 		});
	 * 		assert.strictEqual(typeof project, 'object');
	 * 	} catch (err) {
	 * 		assert.strictEqual(err instanceof GitLabError, true);
	 * 		assert.strictEqual(err.message, '404 Not Found');
	 * 	}
	 * })();
	 * @license Megatherium Standard License 1.0
	 * @param {int|string} projectIdOrName The ID or the name of the project.
	 * @param {object=} additionalParameters Additional parameters to pass to the {@link https://docs.gitlab.com/ee/api/projects.html#get-single-project|GitLab API}.
	 * @returns {Promise<object>} Returns the project's data.
	 * @see {@link https://docs.gitlab.com/ee/api/projects.html#get-single-project|GitLab API: GET /projects/:projectIdOrName}
	 * @throws {module:GitLabError} With "404 Not Found" if the project could not be found.
	 */
	getProject,

	/**
	 * An error resulting from the GitLab API.
	 * @access public
	 * @augments Error
	 * @author Martin Bories <martin.bories@megatherium.solutions>
	 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
	 * @example <caption>Throwing a GitLabError</caption>
	 * import assert from 'assert';
	 * import {
	 * 	GitLabError,
	 * } from '@megatherium/gitlab-api';
	 *
	 * const error = new GitLabError('GET', '/users', {}, {
	 * 	error: {
	 * 		parameters: 'Missing',
	 * 		permission: [
	 * 			'Access denied',
	 * 		],
	 * 	},
	 * });
	 * assert.throws(() => {
	 * 	throw error;
	 * }, error);
	 * @example <caption>Throwing a GitLabError</caption>
	 * import assert from 'assert';
	 * import {
	 * 	GitLabError,
	 * } from '@megatherium/gitlab-api';
	 *
	 * const error = new GitLabError('GET', '/users', {}, {
	 * 	error: {
	 * 		parameters: 'Missing',
	 * 	},
	 * });
	 * assert.throws(() => {
	 * 	throw error;
	 * }, error);
	 * @license Megatherium Standard License 1.0
	 */
	GitLabError,

	/**
	 * Lists all projects from the current namespace, optionally filtered by parameters.
	 * @access public
	 * @augments request
	 * @author Martin Bories <martin.bories@megatherium.solutions>
	 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
	 * @example <caption>List all projects</caption>
	 * import assert from 'assert';
	 * import {
	 * 	listProjects,
	 * } from '@megatherium/gitlab-api';
	 *
	 * (async() => {
	 * 	const projects = await listProjects();
	 *
	 * 	assert.strictEqual(Array.isArray(projects), true);
	 * 	projects.forEach((project) => {
	 * 		assert.strictEqual(typeof project, 'object');
	 * 	});
	 * })();
	 * @license Megatherium Standard License 1.0
	 * @param {object=} additionalParameters Additional search parameters to send to the {@link https://docs.gitlab.com/ee/api/projects.html#list-all-projects|GitLab API}.
	 * @returns {Promise<Array<object>>} Returns an Array with the data of the projects.
	 * @see {@link https://docs.gitlab.com/ee/api/projects.html#list-all-projects|GitLab API: GET /projects}
	 */
	listProjects,

	/**
	 * Lists all the members of a project.
	 * @access public
	 * @augments request
	 * @author Martin Bories <martin.bories@megatherium.solutions>
	 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
	 * @example <caption>List all members of the sample-project</caption>
	 * import assert from 'assert';
	 * import {
	 * 	GitLabError,
	 * 	listProjectMembers,
	 * } from '@megatherium/gitlab-api';
	 *
	 * (async() => {
	 * 	try {
	 * 		const members = await listProjectMembers('sample');
	 *
	 * 		assert.strictEqual(Array.isArray(members), true);
	 * 		members.forEach((member) => {
	 * 			assert.strictEqual(typeof member, 'object');
	 * 		});
	 * 	} catch (err) {
	 * 		assert.strictEqual(err instanceof GitLabError, true);
	 * 		assert.strictEqual(err.message, '404 Project Not Found');
	 * 	}
	 * })();
	 * @license Megatherium Standard License 1.0
	 * @param {int|string} projectIdOrName The ID or the name of the project.
	 * @param {object=} additionalParameters Additional parameters sent to {@link https://docs.gitlab.com/ee/api/members.html#list-all-members-of-a-group-or-project|GET /projects/:projectIdOrName/members}.
	 * @returns {Promise<Array<object>>} Returns an Array with the members.
	 * @see {@link https://docs.gitlab.com/ee/api/members.html#list-all-members-of-a-group-or-project|GitLab API: GET /projects/:projectIdOrName/members}
	 * @throws {module:GitLabError} With "404 Project Not Found" if the project could not be found.
	 */
	listProjectMembers,

	/**
	 * Lists the pipeline schedules from a specific project.
	 * @access public
	 * @augments request
	 * @author Martin Bories <martin.bories@megatherium.solutions>
	 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
	 * @example <caption>List all schedules from a sample project</caption>
	 * import assert from 'assert';
	 * import {
	 * 	GitLabError,
	 * 	listProjectPipelineSchedules,
	 * } from '@megatherium/gitlab-api';
	 *
	 * (async() => {
	 * 	try {
	 * 		const schedules = await listProjectPipelineSchedules('sample');
	 *
	 * 		assert.strictEqual(Array.isArray(schedules), true);
	 * 	} catch (err) {
	 * 		assert.strictEqual(err instanceof GitLabError, true);
	 * 		assert.strictEqual(err.message, '404 Project Not Found');
	 * 	}
	 * })();
	 * @license Megatherium Standard License 1.0
	 * @param {int|string} projectIdOrName The ID or the name of the project.
	 * @param {object=} additionalParameters Additional parameters sent to {@link https://docs.gitlab.com/ee/api/pipeline_schedules.html#get-all-pipeline-schedules|GET /projects/:projectIdOrName/pipeline_schedules}.
	 * @returns {Promise<Array<object>>} Returns an array of the schedules.
	 * @see {@link https://docs.gitlab.com/ee/api/pipeline_schedules.html#get-all-pipeline-schedules|GitLab API: GET /projects/:projectIdOrName/pipeline_schedules}
	 * @throws {module:GitLabError} With "404 Project Not Found" if the project could not be found.
	 */
	listProjectPipelineSchedules,

	/**
	 * Lists the variables added to a project.
	 * @access public
	 * @augments request
	 * @author Martin Bories <martin.bories@megatherium.solutions>
	 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
	 * @example <caption>Get the variables from a sample project</caption>
	 * import assert from 'assert';
	 * import {
	 * 	GitLabError,
	 * 	listProjectVariables,
	 * } from '@megatherium/gitlab-api';
	 *
	 * (async() => {
	 * 	try {
	 * 		const variables = await listProjectVariables('sample');
	 *
	 * 		assert.strictEqual(Array.isArray(variables), true);
	 * 		variables.forEach((variable) => {
	 * 			assert.strictEqual(typeof variable, 'object');
	 * 		});
	 * 	} catch (err) {
	 * 		assert.strictEqual(err instanceof GitLabError, true);
	 * 		assert.strictEqual(err.message, '404 Project Not Found');
	 * 	}
	 * })();
	 * @license Megatherium Standard License 1.0
	 * @param {int|string} projectIdOrName The id or the name of the project.
	 * @returns {Promise<Array<object>>} Returns an array with the variables as received from the {@link https://docs.gitlab.com/ee/api/project_level_variables.html|API}.
	 * @see {@link https://docs.gitlab.com/ee/api/project_level_variables.html|GitLab API: GET /projects/:id/variables}
	 * @throws {module:GitLabError} With "404 Project Not Found" if the project could not be found.
	 */
	listProjectVariables,

	/**
	 * Lists the ssh keys from the current user.
	 * @access public
	 * @augments request
	 * @author Martin Bories <martin.bories@megatherium.solutions>
	 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
	 * @example <caption>List all ssh keys from the current user</caption>
	 * import assert from 'assert';
	 * import {
	 * 	listUserKeys,
	 * } from '@megatherium/gitlab-api';
	 *
	 * (async() => {
	 * 	const keys = await listUserKeys();
	 *
	 * 	assert.strictEqual(Array.isArray(keys), true);
	 * 	keys.forEach((key) => {
	 * 		assert.strictEqual(typeof key, 'object');
	 * 	});
	 * })();
	 * @license Megatherium Standard License 1.0
	 * @returns {Promise<Array<object>>} Returns an array with the data from the user's SSH keys.
	 * @see {@link https://docs.gitlab.com/ee/api/users.html#list-ssh-keys|GitLab API: GET /user/keys}
	 */
	listUserKeys,

	/**
	 * Lists the users. Can be filtered.
	 * @access public
	 * @augments request
	 * @author Martin Bories <martin.bories@megatherium.solutions>
	 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
	 * @example <caption>Search for megatherium</caption>
	 * import assert from 'assert';
	 * import {
	 * 	listUsers,
	 * } from '@megatherium/gitlab-api';
	 *
	 * (async() => {
	 * 	const users = await listUsers({
	 * 		username: 'megatherium',
	 * 	});
	 *
	 * 	assert.strictEqual(Array.isArray(users), true);
	 * 	assert.strictEqual(users.length, 1);
	 * 	assert.strictEqual(users[0].username, 'megatherium');
	 * })();
	 * @example <caption>List all users</caption>
	 * import assert from 'assert';
	 * import {
	 * 	listUsers,
	 * } from '@megatherium/gitlab-api';
	 *
	 * (async() => {
	 * 	const users = await listUsers();
	 *
	 * 	assert.strictEqual(Array.isArray(users), true);
	 * 	assert.strictEqual(users.length > 0, true);
	 * })();
	 * @license Megatherium Standard License 1.0
	 * @param {object=} additionalParameters Additional parameters sent to {@link https://docs.gitlab.com/ee/api/users.html#list-users|GET /users}.
	 * @returns {Promise<Array<object>>} Returns the users.
	 * @see {@link https://docs.gitlab.com/ee/api/users.html#list-users|GitLab API: GET /users}
	 */
	listUsers,

	/**
	 * Parses the given project identifier into a value usable in the HTTP request.
	 *
	 * If the given identifier contains a project name (string), it will be encoded as an URI component.
	 * @access public
	 * @augments encodeURIComponent
	 * @author Martin Bories <martin.bories@megatherium.solutions>
	 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
	 * @example <caption>Parse a project name</caption>
	 * import assert from 'assert';
	 * import {
	 * 	parseProjectIdOrName,
	 * } from '@megatherium/gitlab-api';
	 *
	 * assert.strictEqual(parseProjectIdOrName('my project'), 'my%20project');
	 * @example <caption>Parse a project ID</caption>
	 * import assert from 'assert';
	 * import {
	 * 	parseProjectIdOrName,
	 * } from '@megatherium/gitlab-api';
	 *
	 * assert.strictEqual(parseProjectIdOrName(1337), 1337);
	 * @license Megatherium Standard License 1.0
	 * @param {string|int} projectIdOrName The ID of the project or it's name.
	 * @returns {string|int} Returns the encoded project identifier.
	 */
	parseProjectIdOrName,

	/**
	 * Deletes a project permanently.
	 * @access public
	 * @augments request
	 * @author Martin Bories <martin.bories@megatherium.solutions>
	 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
	 * @example <caption>Delete the sample project</caption>
	 * import {
	 * 	removeProject,
	 * } from '@megatherium/gitlab-api';
	 *
	 * (async() => {
	 * 	await removeProject('sample');
	 * })();
	 * @license Megatherium Standard License 1.0
	 * @param {int|string} projectIdOrName The ID or the name of the project to delete.
	 * @returns {Promise} Resolves after the project has been deleted.
	 * @see {@link https://docs.gitlab.com/ee/api/projects.html#remove-project|GitLab API: DELETE /projects/:projectIdOrName}
	 */
	removeProject,

	/**
	 * Removes a member from a project.
	 * @access public
	 * @augments request
	 * @author Martin Bories <martin.bories@megatherium.solutions>
	 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
	 * @example <caption>Remove all users from the sample-project</caption>
	 * //global Promise
	 * import assert from 'assert';
	 * import {
	 * 	GitLabError,
	 * 	listProjectMembers,
	 * 	removeProjectMember,
	 * } from '@megatherium/gitlab-api';
	 *
	 * (async() => {
	 * 	let members = [];
	 * 	try {
	 * 		members = await listProjectMembers('sample');
	 * 	} catch (err) {
	 * 		assert.strictEqual(err instanceof GitLabError, true);
	 * 		assert.strictEqual(err.message, '404 Project Not Found');
	 * 	}
	 *
	 * 	await Promise.all(members.map(async(member) => {
	 * 		await removeProjectMember('sample', member.id);
	 * 	}));
	 * })();
	 * @example <caption>Remove a specific user</caption>
	 * import {
	 * 	removeProjectMember,
	 * } from '@megatherium/gitlab-api';
	 *
	 * (async() => {
	 * 	await removeProjectMember('sample', 1337);
	 * })();
	 * @license Megatherium Standard License 1.0
	 * @param {int|string} projectIdOrName The ID or the name of the project.
	 * @param {int} userId The ID of the user to remove.
	 * @param {object=} additionalParameters Additional parameters sent to {@link https://docs.gitlab.com/ee/api/members.html#remove-a-member-from-a-group-or-project|DELETE /projects/:projectIdOrName/members/:userId}.
	 * @returns {Promise} Resolves after the member has been removed.
	 * @see {@link https://docs.gitlab.com/ee/api/members.html#remove-a-member-from-a-group-or-project|GitLab API: DELETE /projects/:projectIdOrName/members/:userId}
	 */
	removeProjectMember,

	/**
	 * Removes a pipeline schedule from a project.
	 * @access public
	 * @augments request
	 * @author Martin Bories <martin.bories@megatherium.solutions>
	 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
	 * @example <caption>Remove all pipeline schedules</caption>
	 * //global Promise
	 * import assert from 'assert';
	 * import {
	 * 	GitLabError,
	 * 	listProjectPipelineSchedules,
	 * 	removeProjectPipelineSchedule,
	 * } from '@megatherium/gitlab-api';
	 *
	 * (async() => {
	 * 	try {
	 * 		const schedules = await listProjectPipelineSchedules('sample');
	 *
	 * 		await Promise.all(schedules.map(async(schedule) => {
	 * 			await removeProjectPipelineSchedule('sample', schedule.id);
	 * 		}));
	 * 	} catch (err) {
	 * 		assert.strictEqual(err instanceof GitLabError, true);
	 * 		assert.strictEqual(err.message, '404 Project Not Found');
	 * 	}
	 * })();
	 * @example <caption>Remove a specific pipeline schedule</caption>
	 * import assert from 'assert';
	 * import {
	 * 	GitLabError,
	 * 	removeProjectPipelineSchedule,
	 * } from '@megatherium/gitlab-api';
	 *
	 * (async() => {
	 * 	try {
	 * 		await removeProjectPipelineSchedule('sample', 1337);
	 * 	} catch (err) {
	 * 		assert.strictEqual(err instanceof GitLabError, true);
	 * 		assert.strictEqual(err.message, '404 Project Not Found');
	 * 	}
	 * })();
	 * @license Megatherium Standard License 1.0
	 * @param {int|string} projectIdOrName The ID or name of the project.
	 * @param {int} pipelineScheduleId The ID of the pipeline schedule to delete.
	 * @returns {Promise} Resolves after the project has been deleted.
	 * @see {@link https://docs.gitlab.com/ee/api/pipeline_schedules.html#delete-a-pipeline-schedule|GitLab API: DELETE /projects/:projectIdOrName/pipeline_schedules/:pipelineScheduleId}
	 * @throws {module:GitLabError} With "404 Project Not Found" if the project could not be found.
	 */
	removeProjectPipelineSchedule,

	/**
	 * Deletes a variable from a project.
	 * @access public
	 * @augments request
	 * @author Martin Bories <martin.bories@megatherium.solutions>
	 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
	 * @example <caption>Remove the GITLAB_TOKEN from the sample project</caption>
	 * import {
	 * 	removeProjectVariable,
	 * } from '@megatherium/gitlab-api';
	 *
	 * (async() => {
	 * 	await removeProjectVariable('sample', 'GITLAB_TOKEN');
	 * })();
	 * @license Megatherium Standard License 1.0
	 * @param {int|string} projectIdOrName The ID or the name of the project.
	 * @param {string} variableName The name of the variable to delete.
	 * @param {object=} additionalParameters Additional parameters to send to the {@link https://docs.gitlab.com/ee/api/project_level_variables.html#remove-variable|GitLab API}.
	 * @returns {Promise} Resolves after the variable has been deleted.
	 * @see {@link https://docs.gitlab.com/ee/api/project_level_variables.html#remove-variable|GitLab API: DELETE /projects/:projectIdOrName/variables/:variableName}
	 */
	removeProjectVariable,

	/**
	 * Deletes a key of the current user permanently.
	 * @access public
	 * @augments request
	 * @author Martin Bories <martin.bories@megatherium.solutions>
	 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
	 * @example <caption>Delete the workstation-key from the current user</caption>
	 * import {
	 * 	listUserKeys,
	 * 	removeUserKey,
	 * } from '@megatherium/gitlab-api';
	 *
	 * (async() => {
	 * 	const keys = await listUserKeys(),
	 * 		key = keys.find((key) => {
	 * 			return key.title === 'workstation';
	 * 		});
	 * 	if (key) {
	 * 		await removeUserKey(key.id);
	 * 	}
	 * })();
	 * @example <caption>Delete a specific key from the current user</caption>
	 * import {
	 * 	removeUserKey,
	 * } from '@megatherium/gitlab-api';
	 *
	 * (async() => {
	 * 	await removeUserKey(1337);
	 * })();
	 * @license Megatherium Standard License 1.0
	 * @param {int} keyId The ID of the key to delete.
	 * @returns {Promise} Resolves after the key was deleted.
	 * @see {@link https://docs.gitlab.com/ee/api/users.html#list-ssh-keys|GitLab API: DELETE /user/keys/:keyId}
	 */
	removeUserKey,

	/**
	 * Performs an authenticated API request on the GitLab API.
	 * @access public
	 * @augments external:cross-fetch
	 * @author Martin Bories <martin.bories@megatherium.solutions>
	 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
	 * @example <caption>Create a sample project</caption>
	 * import {
	 * 	GitLabError,
	 * 	request,
	 * } from '@megatherium/gitlab-api';
	 *
	 * (async() => {
	 * 	try {
	 * 		await request('POST', '/projects', {
	 * 			name: 'sample',
	 * 		});
	 * 	} catch (err) {
	 * 		if (!(err instanceof GitLabError) || (
	 * 			err.error.name !== 'has already been taken'
	 * 			&& err.error.path !== 'has already been taken'
	 * 		)) {
	 * 			throw err;
	 * 		}
	 * 	}
	 * })();
	 * @license Megatherium Standard License 1.0
	 * @param {string} method The request method.
	 * @param {string} url The request URL, relative to the base url.
	 * @param {object=} parameters The parameters to send to the remote side.
	 * @returns {Promise<object|undefined>} Returns the response from the server, if any.
	 * @throws {Error} Throws an error if parsing the JSON response failed.
	 * @throws {module:GitLabError} Throws a GitLabError if the GitLab API responds with an error.
	 */
	request,

	/**
	 * Updates the member of a project.
	 * @access public
	 * @augments request
	 * @author Martin Bories <martin.bories@megatherium.solutions>
	 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
	 * @example <caption>Set all members to guests</caption>
	 * //global Promise
	 * import assert from 'assert';
	 * import {
	 * 	GitLabError,
	 * 	listProjectMembers,
	 * 	updateProjectMember,
	 * } from '@megatherium/gitlab-api';
	 *
	 * (async() => {
	 * 	let members = [];
	 * 	try {
	 * 		members = await listProjectMembers('sample');
	 * 	} catch (err) {
	 * 		assert.strictEqual(err instanceof GitLabError, true);
	 * 		assert.strictEqual(err.message, '404 Project Not Found');
	 * 	}
	 *
	 * 	await Promise.all(members.map(async(member) => {
	 * 		await updateProjectMember('sample', member.id, 10);
	 * 	}));
	 * })();
	 * @example <caption>Set a specific member to guest</caption>
	 * import assert from 'assert';
	 * import {
	 * 	GitLabError,
	 * 	updateProjectMember,
	 * } from '@megatherium/gitlab-api';
	 *
	 * (async() => {
	 * 	try {
	 * 		await updateProjectMember('sample', 1337, 10);
	 * 	} catch (err) {
	 * 		assert.strictEqual(err instanceof GitLabError, true);
	 * 		assert.strictEqual(err.message, '404 Project Not Found');
	 * 	}
	 * })();
	 * @license Megatherium Standard License 1.0
	 * @param {int|string} projectIdOrName The ID or the name of the project.
	 * @param {int} userId The ID of the user.
	 * @param {int} accessLevel The new access level of the member.
	 *
	 * - 0: No access
	 * - 10: Guest
	 * - 20: Reporter
	 * - 30: Developer
	 * - 40: Maintainer
	 * @param {object=} additionalData Additional data to update the member with and send to {@link https://docs.gitlab.com/ee/api/members.html#edit-a-member-of-a-group-or-project|PUT /projects/:projectIdOrName/members/:userId}.
	 * @returns {Promise<object>} Returns the data of the updated user.
	 * @see {@link https://docs.gitlab.com/ee/api/members.html#edit-a-member-of-a-group-or-project|GitLab API: PUT /projects/:projectIdOrName/members/:userId}
	 */
	updateProjectMember,

	/**
	 * Updates a pipeline schedule of a project.
	 * @access public
	 * @augments request
	 * @author Martin Bories <martin.bories@megatherium.solutions>
	 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
	 * @example <caption>Deactivate all existing schedules</caption>
	 * //global Promise
	 * import assert from 'assert';
	 * import {
	 * 	GitLabError,
	 * 	listProjectPipelineSchedules,
	 * 	updateProjectPipelineSchedule,
	 * } from '@megatherium/gitlab-api';
	 *
	 * (async() => {
	 * 	let schedules = [];
	 * 	try {
	 * 		schedules = await listProjectPipelineSchedules('sample');
	 * 	} catch (err) {
	 * 		assert.strictEqual(err instanceof GitLabError, true);
	 * 		assert.strictEqual(err.message, '404 Project Not Found');
	 * 	}
	 *
	 * 	await Promise.all(schedules.map(async(schedule) => {
	 * 		await updateProjectPipelineSchedule('sample', schedule.id, {
	 * 			active: false,
	 * 		});
	 * 	}));
	 * })();
	 * @example <caption>Deactivate a specific schedule</caption>
	 * import assert from 'assert';
	 * import {
	 * 	GitLabError,
	 * 	updateProjectPipelineSchedule,
	 * } from '@megatherium/gitlab-api';
	 *
	 * (async() => {
	 * 	try {
	 * 		await updateProjectPipelineSchedule('sample', 1337, {
	 * 			active: false,
	 * 		});
	 * 	} catch (err) {
	 * 		assert.strictEqual(err instanceof GitLabError, true);
	 * 		assert.strictEqual(err.message, '404 Project Not Found');
	 * 	}
	 * })();
	 * @license Megatherium Standard License 1.0
	 * @param {int|string} projectIdOrName The ID or the name of the project.
	 * @param {int} pipelineScheduleId The ID of the pipeline schedule to update.
	 * @param {object} additionalData Additional data to update the pipeline schedule with and send to {@link https://docs.gitlab.com/ee/api/pipeline_schedules.html#edit-a-pipeline-schedule|PUT /projects/:projectIdOrName/pipeline_schedules/:pipelineScheduleId}.
	 * @returns {Promise} Resolves after the pipeline schedule has been updated.
	 * @see {@link https://docs.gitlab.com/ee/api/pipeline_schedules.html#edit-a-pipeline-schedule|GitLab API: PUT /projects/:projectIdOrName/pipeline_schedules/:pipelineScheduleId}
	 */
	updateProjectPipelineSchedule,

	/**
	 * Updates the data from a project variable.
	 * @access public
	 * @augments request
	 * @author Martin Bories <martin.bories@megatherium.solutions>
	 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
	 * @example <caption>Update the GITLAB_TOKEN</caption>
	 * //global process
	 * import assert from 'assert';
	 * import {
	 * 	GitLabError,
	 * 	updateProjectVariable,
	 * } from '@megatherium/gitlab-api';
	 *
	 * (async() => {
	 * 	try {
	 * 		await updateProjectVariable('sample', 'GITLAB_TOKEN', process.env.GITLAB_TOKEN);
	 * 	} catch (err) {
	 * 		assert.strictEqual(err instanceof GitLabError, true);
	 * 		assert.strictEqual(err.message, '404 Project Not Found');
	 *  	}
	 * })();
	 * @license Megatherium Standard License 1.0
	 * @param {int|string} projectIdOrName The id or the name of the project.
	 * @param {string} variableName The name of the variable to update.
	 * @param {string} variableValue The new value of the variable.
	 * @param {object=} additionalData Additional data to add to the key.
	 * @returns {Promise} Resolves after the variable has been updated.
	 * @see {@link https://docs.gitlab.com/ee/api/project_level_variables.html#update-variable|GitLab API: PUT /projects/:projectIdOrName/variables/:variableName}
	 * @throws {module:GitLabError} With "404 Project Not Found" if the project could not be found.
	 */
	updateProjectVariable,
};

export {
	createProject,
	createProjectMember,
	createProjectPipelineSchedule,
	createProjectVariable,
	createUserKey,
	getProject,
	GitLabError,
	listProjectMembers,
	listProjects,
	listProjectPipelineSchedules,
	listProjectVariables,
	listUserKeys,
	listUsers,
	parseProjectIdOrName,
	removeProject,
	removeProjectMember,
	removeProjectPipelineSchedule,
	removeProjectVariable,
	removeUserKey,
	request,
	updateProjectMember,
	updateProjectPipelineSchedule,
	updateProjectVariable,
};

export default gitlab;
