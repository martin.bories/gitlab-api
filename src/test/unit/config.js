/*global describe,it,process,require*/
import assert from 'assert';
import logger from './logger';

const log = logger.init('config.js');

describe(`config.js`, () => {
	it(`should use GITLAB_TOKEN if MEGATHERIUM_GITLAB_ACCESS_TOKEN is not given`, () => {
		const token = process.env.MEGATHERIUM_GITLAB_ACCESS_TOKEN;
		delete process.env.MEGATHERIUM_GITLAB_ACCESS_TOKEN;
		process.env.GITLAB_TOKEN = 'test';

		const config = require('../../config').default;
		assert.strictEqual(config.accessToken, 'test');

		process.env.MEGATHERIUM_GITLAB_ACCESS_TOKEN = token;
		delete process.env.GITLAB_TOKEN;
	});
});
