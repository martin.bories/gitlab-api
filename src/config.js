/*global process*/
const config = {
	accessToken: process.env.MEGATHERIUM_GITLAB_ACCESS_TOKEN || process.env.GITLAB_TOKEN,
	baseUrl: process.env.MEGATHERIUM_GITLAB_BASE_URL || 'https://gitlab.com/api/v4',
};

export default config;
