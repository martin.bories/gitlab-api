import logger from '../logger';
import parseProjectIdOrName from './parseProjectIdOrName';
import request from './request';

const log = logger.init('updateProjectVariable.js');

/**
 * Updates the data from a project variable.
 * @access public
 * @augments request
 * @author Martin Bories <martin.bories@megatherium.solutions>
 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
 * @example <caption>Update the GITLAB_TOKEN</caption>
 * //global process
 * import assert from 'assert';
 * import {
 * 	GitLabError,
 * 	updateProjectVariable,
 * } from '@megatherium/gitlab-api';
 *
 * (async() => {
 * 	try {
 * 		await updateProjectVariable('sample', 'GITLAB_TOKEN', process.env.GITLAB_TOKEN);
 * 	} catch (err) {
 * 		assert.strictEqual(err instanceof GitLabError, true);
 * 		assert.strictEqual(err.message, '404 Project Not Found');
 * 	}
 * })();
 * @license Megatherium Standard License 1.0
 * @module updateProjectVariable
 * @param {int|string} projectIdOrName The id or the name of the project.
 * @param {string} variableName The name of the variable to update.
 * @param {string} variableValue The new value of the variable.
 * @param {object=} additionalData Additional data to add to the key.
 * @returns {Promise} Resolves after the variable has been updated.
 * @see {@link https://docs.gitlab.com/ee/api/project_level_variables.html#update-variable|GitLab API: PUT /projects/:projectIdOrName/variables/:variableName}
 * @throws {module:GitLabError} With "404 Project Not Found" if the project could not be found.
 */
const updateProjectVariable = async(projectIdOrName, variableName, variableValue, additionalData = {}) => {
	projectIdOrName = parseProjectIdOrName(projectIdOrName);
	await request('PUT', `/projects/${ projectIdOrName }/variables/${ variableName }`, {
		value: variableValue,
		...additionalData,
	});
};

export default updateProjectVariable;
