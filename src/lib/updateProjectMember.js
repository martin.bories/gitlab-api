import logger from './logger';
import parseProjectIdOrName from './parseProjectIdOrName';
import request from './request';

const log = logger.init('updateProjectMember.js');

/**
 * Updates the member of a project.
 * @access public
 * @augments request
 * @author Martin Bories <martin.bories@megatherium.solutions>
 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
 * @example <caption>Set all members to guests</caption>
 * //global Promise
 * import assert from 'assert';
 * import {
 * 	GitLabError,
 * 	listProjectMembers,
 * 	updateProjectMember,
 * } from '@megatherium/gitlab-api';
 *
 * (async() => {
 * 	let members = [];
 * 	try {
 * 		members = await listProjectMembers('sample');
 * 	} catch (err) {
 * 		assert.strictEqual(err instanceof GitLabError, true);
 * 		assert.strictEqual(err.message, '404 Project Not Found');
 * 	}
 *
 * 	await Promise.all(members.map(async(member) => {
 * 		await updateProjectMember('sample', member.id, 10);
 * 	}));
 * })();
 * @example <caption>Set a specific member to guest</caption>
 * import assert from 'assert';
 * import {
 * 	GitLabError,
 * 	updateProjectMember,
 * } from '@megatherium/gitlab-api';
 *
 * (async() => {
 * 	try {
 * 		await updateProjectMember('sample', 1337, 10);
 * 	} catch (err) {
 * 		assert.strictEqual(err instanceof GitLabError, true);
 * 		assert.strictEqual(err.message, '404 Project Not Found');
 * 	}
 * })();
 * @license Megatherium Standard License 1.0
 * @module updateProjectMember
 * @param {int|string} projectIdOrName The ID or the name of the project.
 * @param {int} userId The ID of the user.
 * @param {int} accessLevel The new access level of the member.
 *
 * - 0: No access
 * - 10: Guest
 * - 20: Reporter
 * - 30: Developer
 * - 40: Maintainer
 * @param {object=} additionalData Additional data to update the member with and send to {@link https://docs.gitlab.com/ee/api/members.html#edit-a-member-of-a-group-or-project|PUT /projects/:projectIdOrName/members/:userId}.
 * @returns {Promise<object>} Returns the data of the updated user.
 * @see {@link https://docs.gitlab.com/ee/api/members.html#edit-a-member-of-a-group-or-project|GitLab API: PUT /projects/:projectIdOrName/members/:userId}
 */
const updateProjectMember = async(projectIdOrName, userId, accessLevel, additionalData = {}) => {
	projectIdOrName = parseProjectIdOrName(projectIdOrName);
	return await request('PUT', `/projects/${ projectIdOrName }/members/${ userId }`, {
		access_level: accessLevel,
		...additionalData,
	});
};

export default updateProjectMember;
