import logger from './logger';
import request from './request';

const log = logger.init('createProject.js');

/**
 * Creates a new project.
 * @access public
 * @augments request
 * @author Martin Bories <martin.bories@megatherium.solutions>
 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
 * @example <caption>Create a sample project</caption>
 * import assert from 'assert';
 * import {
 * 	createProject,
 * 	GitLabError,
 * } from '@megatherium/gitlab-api';
 *
 * (async() => {
 * 	try {
 * 		await createProject('sample');
 * 	} catch (err) {
 * 		assert.strictEqual(err instanceof GitLabError, true);
 * 		if (err.error.name !== 'has already been taken'
 * 			&& err.error.path !== 'has already been taken'
 * 		) {
 * 			throw err;
 * 		}
 * 	}
 * })();
 * @license Megatherium Standard License 1.0
 * @module createProject
 * @param {string} pathOrName The name or the path of the project to create.
 * @param {object=} additionalData Additional data to add to the project. See {@link https://docs.gitlab.com/ee/api/projects.html#create-project|GitLab API}.
 * @returns {Promise} Resolves after the project has been created.
 * @see {@link https://docs.gitlab.com/ee/api/projects.html#create-project|GitLab API: POST /projects}
 * @throws {module:GitLabError} With `{name: 'has already been taken'}` if the name is already used by another project.
 * @throws {module:GitLabError} With `{path: 'has already been taken'}` if the name is already used by another project.
 */
const createProject = async(pathOrName, additionalData) => {
	await request('POST', `/projects`, {
		name: pathOrName,
		...additionalData,
	});
};

export default createProject;
