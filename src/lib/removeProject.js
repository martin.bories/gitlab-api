import logger from './logger';
import parseProjectIdOrName from './parseProjectIdOrName';
import request from './request';

const log = logger.init('removeProject.js');

/**
 * Deletes a project permanently.
 * @access public
 * @augments request
 * @author Martin Bories <martin.bories@megatherium.solutions>
 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
 * @example <caption>Delete the sample project</caption>
 * import {
 * 	removeProject,
 * } from '@megatherium/gitlab-api';
 *
 * (async() => {
 * 	await removeProject('sample');
 * })();
 * @license Megatherium Standard License 1.0
 * @module removeProject
 * @param {int|string} projectIdOrName The ID or the name of the project to delete.
 * @returns {Promise} Resolves after the project has been deleted.
 * @see {@link https://docs.gitlab.com/ee/api/projects.html#remove-project|GitLab API: DELETE /projects/:projectIdOrName}
 */
const removeProject = async(projectIdOrName) => {
	projectIdOrName = parseProjectIdOrName(projectIdOrName);
	await request('DELETE', `/projects/${ projectIdOrName }`);
};

export default removeProject;
