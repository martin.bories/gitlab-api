import logger from './logger';
import parseProjectIdOrName from './parseProjectIdOrName';
import request from './request';

const log = logger.init('listProjectPipelineSchedules.js');

/**
 * Lists the pipeline schedules from a specific project.
 * @access public
 * @augments request
 * @author Martin Bories <martin.bories@megatherium.solutions>
 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
 * @example <caption>List all schedules from a sample project</caption>
 * import assert from 'assert';
 * import {
 * 	GitLabError,
 * 	listProjectPipelineSchedules,
 * } from '@megatherium/gitlab-api';
 *
 * (async() => {
 * 	try {
 * 		const schedules = await listProjectPipelineSchedules('sample');
 *
 * 		assert.strictEqual(Array.isArray(schedules), true);
 * 	} catch (err) {
 * 		assert.strictEqual(err instanceof GitLabError, true);
 * 		assert.strictEqual(err.message, '404 Project Not Found');
 * 	}
 * })();
 * @license Megatherium Standard License 1.0
 * @module listProjectPipelineSchedules
 * @param {int|string} projectIdOrName The ID or the name of the project.
 * @param {object=} additionalParameters Additional parameters sent to {@link https://docs.gitlab.com/ee/api/pipeline_schedules.html#get-all-pipeline-schedules|GET /projects/:projectIdOrName/pipeline_schedules}.
 * @returns {Promise<Array<object>>} Returns an array of the schedules.
 * @see {@link https://docs.gitlab.com/ee/api/pipeline_schedules.html#get-all-pipeline-schedules|GitLab API: GET /projects/:projectIdOrName/pipeline_schedules}
 * @throws {module:GitLabError} With "404 Project Not Found" if the project could not be found.
 */
const listProjectPipelineSchedules = async(projectIdOrName, additionalParameters = {}) => {
	projectIdOrName = parseProjectIdOrName(projectIdOrName);
	return await request('GET', `/projects/${ projectIdOrName }/pipeline_schedules`, additionalParameters);
};

export default listProjectPipelineSchedules;
