import logger from './logger';
import parseProjectIdOrName from './parseProjectIdOrName';
import request from './request';

const log = logger.init('removeProjectPipelineSchedule.js');

/**
 * Removes a pipeline schedule from a project.
 * @access public
 * @augments request
 * @author Martin Bories <martin.bories@megatherium.solutions>
 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
 * @example <caption>Remove all pipeline schedules</caption>
 * //global Promise
 * import assert from 'assert';
 * import {
 * 	GitLabError,
 * 	listProjectPipelineSchedules,
 * 	removeProjectPipelineSchedule,
 * } from '@megatherium/gitlab-api';
 *
 * (async() => {
 * 	try {
 * 		const schedules = await listProjectPipelineSchedules('sample');
 *
 * 		await Promise.all(schedules.map(async(schedule) => {
 * 			await removeProjectPipelineSchedule('sample', schedule.id);
 * 		}));
 * 	} catch (err) {
 * 		assert.strictEqual(err instanceof GitLabError, true);
 * 		assert.strictEqual(err.message, '404 Project Not Found');
 * 	}
 * })();
 * @example <caption>Remove a specific pipeline schedule</caption>
 * import assert from 'assert';
 * import {
 * 	GitLabError,
 * 	removeProjectPipelineSchedule,
 * } from '@megatherium/gitlab-api';
 *
 * (async() => {
 * 	try {
 * 		await removeProjectPipelineSchedule('sample', 1337);
 * 	} catch (err) {
 * 		assert.strictEqual(err instanceof GitLabError, true);
 * 		assert.strictEqual(err.message, '404 Project Not Found');
 * 	}
 * })();
 * @license Megatherium Standard License 1.0
 * @module removeProjectPipelineSchedule
 * @param {int|string} projectIdOrName The ID or name of the project.
 * @param {int} pipelineScheduleId The ID of the pipeline schedule to delete.
 * @returns {Promise} Resolves after the project has been deleted.
 * @see {@link https://docs.gitlab.com/ee/api/pipeline_schedules.html#delete-a-pipeline-schedule|GitLab API: DELETE /projects/:projectIdOrName/pipeline_schedules/:pipelineScheduleId}
 * @throws {module:GitLabError} With "404 Project Not Found" if the project could not be found.
 */
const removeProjectPipelineSchedule = async(projectIdOrName, pipelineScheduleId) => {
	projectIdOrName = parseProjectIdOrName(projectIdOrName);
	await request('DELETE', `/projects/${ projectIdOrName }/pipeline_schedules/${ pipelineScheduleId }`);
};

export default removeProjectPipelineSchedule;
