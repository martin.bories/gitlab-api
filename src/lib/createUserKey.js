import logger from './logger';
import request from './request';

const log = logger.init('createSshKey.js');

/**
 * Adds a new ssh key to the current user.
 * @access public
 * @augments request
 * @author Martin Bories <martin.bories@megatherium.solutions>
 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
 * @example <caption>Create the current ssh key</caption>
 * import assert from 'assert';
 * import {
 * 	createUserKey,
 * 	GitLabError,
 * } from '@megatherium/gitlab-api';
 *
 * const publicKey = `ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAAAgQCqGKukO1De7zhZj6+H0qtjTkVxwTCpvKe4eCZ0FPqri0cb2JZfXJ/DgYSF6vUpwmJG8wVQZKjeGcjDOL5UlsuusFncCzWBQ7RKNUSesmQRMSGkVb1/3j+skZ6UtW+5u09lHNsj6tQ51s1SPrCBkedbNf0Tp0GbMJDyR4e9T04ZZw== example`;
 *
 * (async() => {
 * 	try {
 * 		await createUserKey('test-user-key', publicKey);
 * 	} catch (err) {
 * 		assert.strictEqual(err instanceof GitLabError, true);
 * 		assert.strictEqual(err.message, 'has already been taken');
 * 	}
 * })();
 * @license Megatherium Standard License 1.0
 * @module createUserKey
 * @param {string} label The title of the new key.
 * @param {string} publicKey The public key to store.
 * @param {object=} additionalData Additional data to store along with the key. See {@link https://docs.gitlab.com/ee/api/users.html#add-ssh-key|GitLab API}.
 * @returns {Promise} Resolves after the key has been created.
 * @see {@link https://docs.gitlab.com/ee/api/users.html#add-ssh-key|GitLab API: POST /user/keys}
 * @throws {module:GitLabError} With `{fingerprint: 'has already been taken'}` when the fingerprint of the public key has already been added to the current user.
 */
const createUserKey = (label, publicKey, additionalData) => {
	return request('POST', `/user/keys`, {
		title: label,
		key: publicKey,
		...additionalData,
	});
};

export default createUserKey;
