import logger from './logger';
import parseProjectIdOrName from './parseProjectIdOrName';
import request from './request';

const log = logger.init('listProjectMembers.js');

/**
 * Lists all the members of a project.
 * @access public
 * @augments request
 * @author Martin Bories <martin.bories@megatherium.solutions>
 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
 * @example <caption>List all members of the sample-project</caption>
 * import assert from 'assert';
 * import {
 * 	GitLabError,
 * 	listProjectMembers,
 * } from '@megatherium/gitlab-api';
 *
 * (async() => {
 * 	try {
 * 		const members = await listProjectMembers('sample');
 *
 * 		assert.strictEqual(Array.isArray(members), true);
 * 		members.forEach((member) => {
 * 			assert.strictEqual(typeof member, 'object');
 * 		});
 * 	} catch (err) {
 * 		assert.strictEqual(err instanceof GitLabError, true);
 * 		assert.strictEqual(err.message, '404 Project Not Found');
 * 	}
 * })();
 * @license Megatherium Standard License 1.0
 * @module listProjectMembers
 * @param {int|string} projectIdOrName The ID or the name of the project.
 * @param {object=} additionalParameters Additional parameters sent to {@link https://docs.gitlab.com/ee/api/members.html#list-all-members-of-a-group-or-project|GET /projects/:projectIdOrName/members}.
 * @returns {Promise<Array<object>>} Returns an Array with the members.
 * @see {@link https://docs.gitlab.com/ee/api/members.html#list-all-members-of-a-group-or-project|GitLab API: GET /projects/:projectIdOrName/members}
 * @throws {module:GitLabError} With "404 Project Not Found" if the project could not be found.
 */
const listProjectMembers = async(projectIdOrName, additionalParameters = {}) => {
	projectIdOrName = parseProjectIdOrName(projectIdOrName);
	return await request('GET', `/projects/${ projectIdOrName }/members`, additionalParameters);
};

export default listProjectMembers;
