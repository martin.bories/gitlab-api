import logger from './logger';
import request from './request';

const log = logger.init('removeUserKey.js');

/**
 * Deletes a key of the current user permanently.
 * @access public
 * @augments request
 * @author Martin Bories <martin.bories@megatherium.solutions>
 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
 * @example <caption>Delete the workstation-key from the current user</caption>
 * import {
 * 	listUserKeys,
 * 	removeUserKey,
 * } from '@megatherium/gitlab-api';
 *
 * (async() => {
 * 	const keys = await listUserKeys(),
 * 		key = keys.find((key) => {
 * 			return key.title === 'test-user-key';
 * 		});
 * 	if (key) {
 * 		await removeUserKey(key.id);
 * 	}
 * })();
 * @example <caption>Delete a specific key from the current user</caption>
 * import {
 * 	removeUserKey,
 * } from '@megatherium/gitlab-api';
 *
 * (async() => {
 * 	await removeUserKey(1337);
 * })();
 * @license Megatherium Standard License 1.0
 * @module removeUserKey
 * @param {int} keyId The ID of the key to delete.
 * @returns {Promise} Resolves after the key was deleted.
 * @see {@link https://docs.gitlab.com/ee/api/users.html#list-ssh-keys|GitLab API: DELETE /user/keys/:keyId}
 */
const removeUserKey = (keyId) => {
	return request('DELETE', `/user/keys/${ keyId }`);
};

export default removeUserKey;
