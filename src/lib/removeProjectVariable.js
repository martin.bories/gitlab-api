import logger from '../logger';
import parseProjectIdOrName from './parseProjectIdOrName';
import request from './request';

const log = logger.init('removeProjectVariable.js');

/**
 * Deletes a variable from a project.
 * @access public
 * @augments request
 * @author Martin Bories <martin.bories@megatherium.solutions>
 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
 * @example <caption>Remove the GITLAB_TOKEN from the sample project</caption>
 * import {
 * 	removeProjectVariable,
 * } from '@megatherium/gitlab-api';
 *
 * (async() => {
 * 	await removeProjectVariable('sample', 'GITLAB_TOKEN');
 * })();
 * @license Megatherium Standard License 1.0
 * @module removeProjectVariable
 * @param {int|string} projectIdOrName The ID or the name of the project.
 * @param {string} variableName The name of the variable to delete.
 * @param {object=} additionalParameters Additional parameters to send to the {@link https://docs.gitlab.com/ee/api/project_level_variables.html#remove-variable|GitLab API}.
 * @returns {Promise} Resolves after the variable has been deleted.
 * @see {@link https://docs.gitlab.com/ee/api/project_level_variables.html#remove-variable|GitLab API: DELETE /projects/:projectIdOrName/variables/:variableName}
 */
const removeProjectVariable = async(projectIdOrName, variableName, additionalParameters = {}) => {
	projectIdOrName = parseProjectIdOrName(projectIdOrName);
	await request('DELETE', `/projects/${ projectIdOrName }/variables/${ variableName }`, additionalParameters);
};

export default removeProjectVariable;
