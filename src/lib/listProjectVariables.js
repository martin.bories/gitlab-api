import logger from '../logger';
import parseProjectIdOrname from './parseProjectIdOrName';
import request from './request';

const log = logger.init('listProjectVariables.js');

/**
 * Lists the variables added to a project.
 * @access public
 * @augments request
 * @author Martin Bories <martin.bories@megatherium.solutions>
 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
 * @example <caption>Get the variables from a sample project</caption>
 * import assert from 'assert';
 * import {
 * 	GitLabError,
 * 	listProjectVariables,
 * } from '@megatherium/gitlab-api';
 *
 * (async() => {
 * 	try {
 * 		const variables = await listProjectVariables('sample');
 *
 * 		assert.strictEqual(Array.isArray(variables), true);
 * 		variables.forEach((variable) => {
 * 			assert.strictEqual(typeof variable, 'object');
 * 		});
 * 	} catch (err) {
 * 		assert.strictEqual(err instanceof GitLabError, true);
 * 		assert.strictEqual(err.message, '404 Project Not Found');
 * 	}
 * })();
 * @license Megatherium Standard License 1.0
 * @module listProjectVariables
 * @param {int|string} projectIdOrName The id or the name of the project.
 * @returns {Promise<Array<object>>} Returns an array with the variables as received from the {@link https://docs.gitlab.com/ee/api/project_level_variables.html|API}.
 * @see {@link https://docs.gitlab.com/ee/api/project_level_variables.html|GitLab API: GET /projects/:id/variables}
 * @throws {module:GitLabError} With "404 Project Not Found" if the project could not be found.
 */
const listProjectVariables = async(projectIdOrName) => {
	projectIdOrName = parseProjectIdOrname(projectIdOrName);
	return await request('GET', `/projects/${ projectIdOrName }/variables`);
};

export default listProjectVariables;
