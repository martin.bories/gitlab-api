import logger from './logger';
import request from './request';

const log = logger.init('listUserKeys.js');

/**
 * Lists the ssh keys from the current user.
 * @access public
 * @augments request
 * @author Martin Bories <martin.bories@megatherium.solutions>
 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
 * @example <caption>List all ssh keys from the current user</caption>
 * import assert from 'assert';
 * import {
 * 	listUserKeys,
 * } from '@megatherium/gitlab-api';
 *
 * (async() => {
 * 	const keys = await listUserKeys();
 *
 * 	assert.strictEqual(Array.isArray(keys), true);
 * 	keys.forEach((key) => {
 * 		assert.strictEqual(typeof key, 'object');
 * 	});
 * })();
 * @license Megatherium Standard License 1.0
 * @module listUserKeys
 * @returns {Promise<Array<object>>} Returns an array with the data from the user's SSH keys.
 * @see {@link https://docs.gitlab.com/ee/api/users.html#list-ssh-keys|GitLab API: GET /user/keys}
 */
const listUserKeys = () => {
	return request('GET', `/user/keys`);
};

export default listUserKeys;
