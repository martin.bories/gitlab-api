import config from '../config';
import fetch from 'cross-fetch';
import GitLabError from './GitLabError';
import logger from './logger';
import qs from 'qs';

const log = logger.init('request.js');

/**
 * Performs an authenticated API request on the GitLab API.
 * @access public
 * @augments external:cross-fetch
 * @author Martin Bories <martin.bories@megatherium.solutions>
 * @copyright 2021 Megatherium UG (haftungsbeschränkt)
 * @example <caption>Create a sample project</caption>
 * import {
 * 	GitLabError,
 * 	request,
 * } from '@megatherium/gitlab-api';
 *
 * (async() => {
 * 	try {
 * 		await request('POST', '/projects', {
 * 			name: 'sample',
 * 		});
 * 	} catch (err) {
 * 		if (!(err instanceof GitLabError) || (
 * 			err.error.name !== 'has already been taken'
 * 			&& err.error.path !== 'has already been taken'
 * 		)) {
 * 			throw err;
 * 		}
 * 	}
 * })();
 * @example <caption>Requesting a non-existant resource</caption>
 * import {
 * 	GitLabError,
 * 	request,
 * } from '@megatherium/gitlab-api';
 *
 * (async() => {
 * 	try {
 * 		await request('GET', '/projects12345', {
 * 			name: 'sample',
 * 		});
 * 	} catch (err) {
 * 		if (!(err instanceof GitLabError) || err.message.indexOf('404') < 0) {
 * 			throw err;
 * 		}
 * 	}
 * })();
 * @license Megatherium Standard License 1.0
 * @module request
 * @param {string} method The request method.
 * @param {string} url The request URL, relative to the base url.
 * @param {object=} parameters The parameters to send to the remote side.
 * @returns {Promise<object|undefined>} Returns the response from the server, if any.
 * @throws {Error} Throws an error if parsing the JSON response failed.
 * @throws {module:GitLabError} Throws a GitLabError if the GitLab API responds with an error.
 */
const request = async(method, url, parameters = {}) => {
	const options = {
		method,
		headers: {
			Accept: 'application/json',
			Authorization: `Bearer ${ config.accessToken }`,
			'Content-Type': 'application/json',
		},
	};
	if (parameters && Object.keys(parameters).length > 0) {
		if (method === 'GET') {
			url += `?${ qs.stringify(parameters) }`;
		} else {
			options.body = JSON.stringify(parameters);
		}
	}
	const response = await fetch(`${ config.baseUrl }${ url }`, options);
	if (method !== 'DELETE') {
		let data = await response.text();

		try {
			data = JSON.parse(data);
		} catch (err) {
			throw new GitLabError(method, url, parameters, data);
		}

		if (!response.ok) {
			throw new GitLabError(method, url, parameters, data);
		}

		return data;
	}
	return response.text();
};

export default request;
