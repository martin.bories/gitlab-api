import logger from './logger';
import parseProjectIdOrName from './parseProjectIdOrName';
import request from './request';

const log = logger.init('createProjectPipelineSchedule.js');

/**
 * Creates a new pipeline schedule for a project.
 * @access public
 * @augments request
 * @author Martin Bories <martin.bories@megatherium.solutions>
 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
 * @example <caption>Create an hourly schedule</caption>
 * import assert from 'assert';
 * import {
 * 	createProjectPipelineSchedule,
 * 	GitLabError,
 * } from '@megatherium/gitlab-api';
 *
 * (async() => {
 * 	try {
 * 		await createProjectPipelineSchedule('sample', 'Run build hourly from 8 AM to 10 PM.', 'master', '0 8-22 * *', {
 * 			cron_timezone: 'UTC+1',
 * 		});
 * 	} catch (err) {
 * 		assert.strictEqual(err instanceof GitLabError, true);
 * 		assert.strictEqual(err.message, '404 Project Not Found');
 * 	}
 * })();
 * @example <caption>Create an hourly schedule in default timezone</caption>
 * import assert from 'assert';
 * import {
 * 	createProjectPipelineSchedule,
 * 	GitLabError,
 * } from '@megatherium/gitlab-api';
 *
 * (async() => {
 * 	try {
 * 		await createProjectPipelineSchedule('sample', 'Run build hourly from 8 AM to 10 PM.', 'master', '0 8-22 * *');
 * 	} catch (err) {
 * 		assert.strictEqual(err instanceof GitLabError, true);
 * 		assert.strictEqual(err.message, '404 Project Not Found');
 * 	}
 * })();
 * @license Megatherium Standard License 1.0
 * @module createProjectPipelineSchedule
 * @param {int|string} projectIdOrName The ID of the project or it's name.
 * @param {string} description The description of the pipeline schedule.
 * @param {string} ref The reference to the branch to execute on, e.g. "master".
 * @param {string} cron The cronjob configuration. See: {@link https://en.wikipedia.org/wiki/Cron|Wikipedia: Cron Syntax}
 * @param {object=} additionalData Additional data to add to the project. See {@link https://docs.gitlab.com/ee/api/pipeline_schedules.html#create-a-new-pipeline-schedule|POST /projects/:projectIdOrName/pipeline_schedules}.
 * @returns {Promise} Resolves after the pipeline schedule for the project has been created.
 * @see {@link https://docs.gitlab.com/ee/api/pipeline_schedules.html#create-a-new-pipeline-schedule|GitLab API: POST /projects/:projectIdOrName/pipeline_schedules}
 * @see {@link https://en.wikipedia.org/wiki/Cron|Wikipedia: Cron Syntax}
 * @throws {module:GitLabError} With "404 Project Not Found" if the project could not be found.
 */
const createProjectPipelineSchedule = async(projectIdOrName, description, ref, cron, additionalData = {}) => {
	projectIdOrName = parseProjectIdOrName(projectIdOrName);
	await request('POST', `/projects/${ projectIdOrName }/pipeline_schedules`, {
		description,
		ref,
		cron,
		...additionalData,
	});
};

export default createProjectPipelineSchedule;
