import logger from './logger';
import parseProjectIdOrName from './parseProjectIdOrName';
import request from './request';

const log = logger.init('removeProjectMember.js');

/**
 * Removes a member from a project.
 * @access public
 * @augments request
 * @author Martin Bories <martin.bories@megatherium.solutions>
 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
 * @example <caption>Remove all users from the sample-project</caption>
 * //global Promise
 * import assert from 'assert';
 * import {
 * 	GitLabError,
 * 	listProjectMembers,
 * 	removeProjectMember,
 * } from '@megatherium/gitlab-api';
 *
 * (async() => {
 * 	let members = [];
 * 	try {
 * 		members = await listProjectMembers('sample');
 * 	} catch (err) {
 * 		assert.strictEqual(err instanceof GitLabError, true);
 * 		assert.strictEqual(err.message, '404 Project Not Found');
 * 	}
 *
 * 	await Promise.all(members.map(async(member) => {
 * 		await removeProjectMember('sample', member.id);
 * 	}));
 * })();
 * @example <caption>Remove a specific user</caption>
 * import {
 * 	removeProjectMember,
 * } from '@megatherium/gitlab-api';
 *
 * (async() => {
 * 	await removeProjectMember('sample', 1337);
 * })();
 * @license Megatherium Standard License 1.0
 * @module removeProjectMember
 * @param {int|string} projectIdOrName The ID or the name of the project.
 * @param {int} userId The ID of the user to remove.
 * @param {object=} additionalParameters Additional parameters sent to {@link https://docs.gitlab.com/ee/api/members.html#remove-a-member-from-a-group-or-project|DELETE /projects/:projectIdOrName/members/:userId}.
 * @returns {Promise} Resolves after the member has been removed.
 * @see {@link https://docs.gitlab.com/ee/api/members.html#remove-a-member-from-a-group-or-project|GitLab API: DELETE /projects/:projectIdOrName/members/:userId}
 */
const removeProjectMember = async(projectIdOrName, userId, additionalParameters = {}) => {
	projectIdOrName = parseProjectIdOrName(projectIdOrName);
	await request('DELETE', `/projects/${ projectIdOrName }/members/${ userId }`, additionalParameters);
};

export default removeProjectMember;
