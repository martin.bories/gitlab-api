import logger from './logger';
import parseProjectIdOrName from './parseProjectIdOrName';
import request from './request';

const log = logger.init('updateProjectPipelineSchedule.js');

/**
 * Updates a pipeline schedule of a project.
 * @access public
 * @augments request
 * @author Martin Bories <martin.bories@megatherium.solutions>
 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
 * @example <caption>Deactivate all existing schedules</caption>
 * //global Promise
 * import assert from 'assert';
 * import {
 * 	GitLabError,
 * 	listProjectPipelineSchedules,
 * 	updateProjectPipelineSchedule,
 * } from '@megatherium/gitlab-api';
 *
 * (async() => {
 * 	let schedules = [];
 * 	try {
 * 		schedules = await listProjectPipelineSchedules('sample');
 * 	} catch (err) {
 * 		assert.strictEqual(err instanceof GitLabError, true);
 * 		assert.strictEqual(err.message, '404 Project Not Found');
 * 	}
 *
 * 	await Promise.all(schedules.map(async(schedule) => {
 * 		await updateProjectPipelineSchedule('sample', schedule.id, {
 * 			active: false,
 * 		});
 * 	}));
 * })();
 * @example <caption>Deactivate a specific schedule</caption>
 * import assert from 'assert';
 * import {
 * 	GitLabError,
 * 	updateProjectPipelineSchedule,
 * } from '@megatherium/gitlab-api';
 *
 * (async() => {
 * 	try {
 * 		await updateProjectPipelineSchedule('sample', 1337, {
 * 			active: false,
 * 		});
 * 	} catch (err) {
 * 		assert.strictEqual(err instanceof GitLabError, true);
 * 		assert.strictEqual(err.message, '404 Project Not Found');
 * 	}
 * })();
 * @license Megatherium Standard License 1.0
 * @module updateProjectPipelineSchedule
 * @param {int|string} projectIdOrName The ID or the name of the project.
 * @param {int} pipelineScheduleId The ID of the pipeline schedule to update.
 * @param {object} additionalData Additional data to update the pipeline schedule with and send to {@link https://docs.gitlab.com/ee/api/pipeline_schedules.html#edit-a-pipeline-schedule|PUT /projects/:projectIdOrName/pipeline_schedules/:pipelineScheduleId}.
 * @returns {Promise} Resolves after the pipeline schedule has been updated.
 * @see {@link https://docs.gitlab.com/ee/api/pipeline_schedules.html#edit-a-pipeline-schedule|GitLab API: PUT /projects/:projectIdOrName/pipeline_schedules/:pipelineScheduleId}
 */
const updateProjectPipelineSchedule = async(projectIdOrName, pipelineScheduleId, additionalData) => {
	projectIdOrName = parseProjectIdOrName(projectIdOrName);
	await request('PUT', `/projects/${ projectIdOrName }/pipeline_schedules/${ pipelineScheduleId }`, additionalData);
};

export default updateProjectPipelineSchedule;
