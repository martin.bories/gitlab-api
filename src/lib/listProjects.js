import logger from './logger';
import request from './request';

const log = logger.init('listProjects.js');

/**
 * Lists all projects from the current namespace, optionally filtered by parameters.
 * @access public
 * @augments request
 * @author Martin Bories <martin.bories@megatherium.solutions>
 * @copyright 2021 Megatherium UG (haftungsbeschränkt)
 * @example <caption>List all owned projects</caption>
 * import assert from 'assert';
 * import {
 * 	listProjects,
 * } from '@megatherium/gitlab-api';
 *
 * (async() => {
 * 	const projects = await listProjects({
 * 		archived: false,
 * 		owned: true,
 * 	});
 *
 * 	assert.strictEqual(Array.isArray(projects), true);
 * 	projects.forEach((project) => {
 * 		assert.strictEqual(typeof project, 'object');
 * 	});
 * })();
 * @license Megatherium Standard License 1.0
 * @module listProjects
 * @param {object=} additionalParameters Additional search parameters to send to the {@link https://docs.gitlab.com/ee/api/projects.html#list-all-projects|GitLab API}.
 * @returns {Promise<Array<object>>} Returns an Array with the data of the projects.
 * @see {@link https://docs.gitlab.com/ee/api/projects.html#list-all-projects|GitLab API: GET /projects}
 */
const listProjects = async(additionalParameters = {}) => {
	return await request('GET', `/projects`, additionalParameters);
};

export default listProjects;
