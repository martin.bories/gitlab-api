import logger from './logger';
import parseProjectIdOrName from './parseProjectIdOrName';
import request from './request';

const log = logger.init('createProjectVariable.js');

/**
 * Creates a new variable for a GitLab project.
 * @access public
 * @augments request
 * @author Martin Bories <martin.bories@megatherium.solutions>
 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
 * @example <caption>Create a variable for MEGATHERIUM_NAME</caption>
 * //global process
 * import assert from 'assert';
 * import {
 * 	createProjectVariable,
 * 	GitLabError,
 * } from '@megatherium/gitlab-api';
 *
 * (async() => {
 * 	try {
 * 		await createProjectVariable('gitlab-api', {
 * 			name: 'MEGATHERIUM_NAME',
 * 			value: process.env.MEGATHERIUM_NAME,
 * 		});
 * 	} catch (err) {
 * 		assert.strictEqual(err instanceof GitLabError, true);
 * 		assert.strictEqual(err.message, '404 Project Not Found');
 * 	}
 * })();
 * @license Megatherium Standard License 1.0
 * @module createProjectVariable
 * @param {int|string} projectIdOrName The ID of the project or the name of the project.
 * @param {string} variableName The name of the variable.
 * @param {string} variableValue The value of the variable.
 * @param {object=} additionalData Additional data stored for the variable. See {@link https://docs.gitlab.com/ee/api/project_level_variables.html#create-variable|GitLab API}
 * @returns {Promise} Resolves after the variable has been created.
 * @see {@link https://docs.gitlab.com/ee/api/project_level_variables.html#create-variable|GitLab API: POST /projects/:projectIdOrName/variables}
 * @throws {module:GitLabError} With "404 Project Not Found" if the project could not be found.
 */
const createProjectVariable = async(projectIdOrName, variableName, variableValue, additionalData = {}) => {
	projectIdOrName = parseProjectIdOrName(projectIdOrName);

	return await request('POST', `/projects/${ projectIdOrName }/variables`, {
		key: variableName,
		value: variableValue,
		...additionalData,
	});
};

export default createProjectVariable;
