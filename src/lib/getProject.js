import logger from './logger';
import parseProjectIdOrName from './parseProjectIdOrName';
import request from './request';

const log = logger.init('getProject.js');

/**
 * Retrieves the information about a project.
 * @access public
 * @augments request
 * @author Martin Bories <martin.bories@megatherium.solutions>
 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
 * @example <caption>Get the data from the sample project</caption>
 * import assert from 'assert';
 * import {
 * 	getProject,
 * 	GitLabError,
 * } from '@megatherium/gitlab-api';
 *
 * (async() => {
 * 	try {
 * 		const project = await getProject('sample', {
 * 			statistics: true,
 * 		});
 * 		assert.strictEqual(typeof project, 'object');
 * 	} catch (err) {
 * 		assert.strictEqual(err instanceof GitLabError, true);
 * 		assert.strictEqual(err.message, '404 Not Found');
 * 	}
 * })();
 * @license Megatherium Standard License 1.0
 * @module getProject
 * @param {int|string} projectIdOrName The ID or the name of the project.
 * @param {object=} additionalParameters Additional parameters to pass to the {@link https://docs.gitlab.com/ee/api/projects.html#get-single-project|GitLab API}.
 * @returns {Promise<object>} Returns the project's data.
 * @see {@link https://docs.gitlab.com/ee/api/projects.html#get-single-project|GitLab API: GET /projects/:projectIdOrName}
 * @throws {module:GitLabError} With "404 Not Found" if the project could not be found.
 */
const getProject = (projectIdOrName, additionalParameters) => {
	projectIdOrName = parseProjectIdOrName(projectIdOrName);
	return request('GET', `/project/${ projectIdOrName }`, additionalParameters);
};

export default getProject;
