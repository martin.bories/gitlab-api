import logger from './logger';
import request from './request';

const log = logger.init('listUsers.js');

/**
 * Lists the users. Can be filtered.
 * @access public
 * @augments request
 * @author Martin Bories <martin.bories@megatherium.solutions>
 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
 * @example <caption>Search for megatherium</caption>
 * import assert from 'assert';
 * import {
 * 	listUsers,
 * } from '@megatherium/gitlab-api';
 *
 * (async() => {
 * 	const users = await listUsers({
 * 		username: 'megatherium',
 * 	});
 *
 * 	assert.strictEqual(Array.isArray(users), true);
 * 	assert.strictEqual(users.length, 1);
 * 	assert.strictEqual(users[0].username, 'megatherium');
 * })();
 * @example <caption>List all users</caption>
 * import assert from 'assert';
 * import {
 * 	listUsers,
 * } from '@megatherium/gitlab-api';
 *
 * (async() => {
 * 	const users = await listUsers();
 *
 * 	assert.strictEqual(Array.isArray(users), true);
 * 	assert.strictEqual(users.length > 0, true);
 * })();
 * @license Megatherium Standard License 1.0
 * @module listUsers
 * @param {object=} additionalParameters Additional parameters sent to {@link https://docs.gitlab.com/ee/api/users.html#list-users|GET /users}.
 * @returns {Promise<Array<object>>} Returns the users.
 * @see {@link https://docs.gitlab.com/ee/api/users.html#list-users|GitLab API: GET /users}
 */
const listUsers = async(additionalParameters = {}) => {
	return await request('GET', '/users', additionalParameters);
};

export default listUsers;
