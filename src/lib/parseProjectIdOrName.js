import logger from './logger';

const log = logger.init('parseProjectIdOrName.js');

/**
 * Parses the given project identifier into a value usable in the HTTP request.
 *
 * If the given identifier contains a project name (string), it will be encoded as an URI component.
 * @access public
 * @augments encodeURIComponent
 * @author Martin Bories <martin.bories@megatherium.solutions>
 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
 * @example <caption>Parse a project name</caption>
 * import assert from 'assert';
 * import {
 * 	parseProjectIdOrName,
 * } from '@megatherium/gitlab-api';
 *
 * assert.strictEqual(parseProjectIdOrName('my project'), 'my%20project');
 * @example <caption>Parse a project ID</caption>
 * import assert from 'assert';
 * import {
 * 	parseProjectIdOrName,
 * } from '@megatherium/gitlab-api';
 *
 * assert.strictEqual(parseProjectIdOrName(1337), 1337);
 * @license Megatherium Standard License 1.0
 * @module parseProjectIdOrName
 * @param {string|int} projectIdOrName The ID of the project or it's name.
 * @returns {string|int} Returns the encoded project identifier.
 */
const parseProjectIdOrName = (projectIdOrName) => {
	if (typeof projectIdOrName === 'string') {
		projectIdOrName = encodeURIComponent(projectIdOrName);
	}
	return projectIdOrName;
};

export default parseProjectIdOrName;
