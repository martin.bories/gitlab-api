import logger from './logger';
import parseProjectIdOrName from './parseProjectIdOrName';
import request from './request';

const log = logger.init('createProjectMember.js');

/**
 * Adds a member to a project.
 * @access public
 * @augments request
 * @author Martin Bories <martin.bories@megatherium.solutions>
 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
 * @example <caption>Add a user to the sample-project</caption>
 * import assert from 'assert';
 * import {
 * 	createProjectMember,
 * 	GitLabError,
 * } from '@megatherium/gitlab-api';
 *
 * (async() => {
 * 	try {
 * 		await createProjectMember('sample', 1, 0);
 * 	} catch (err) {
 * 		assert.strictEqual(err instanceof GitLabError, true);
 * 		assert.strictEqual(err.message, '404 Project Not Found');
 * 	}
 * })();
 * @license Megatherium Standard License 1.0
 * @module createProjectMember
 * @param {int|string} projectIdOrName The ID or the name of the project.
 * @param {int} userId The ID of the user.
 * @param {int} accessLevel The access level the user will be granted.
 *
 * - 0: No access
 * - 10: Guest
 * - 20: Reporter
 * - 30: Developer
 * - 40: Maintainer
 * @param {object=} additionalData Additional data sent to {@link https://docs.gitlab.com/ee/api/members.html#add-a-member-to-a-group-or-project|POST /projects/:projectIdOrName/members}.
 * @returns {Promise<object>} Returns the data of the user after he was added as a member to the project.
 * @see {@link https://docs.gitlab.com/ee/api/members.html#add-a-member-to-a-group-or-project|GitLab API: POST /projects/:projectIdOrName/members}
 * @throws {module:GitLabError} With "404 Project Not Found" if the project could not be found.
 */
const createProjectMember = async(projectIdOrName, userId, accessLevel, additionalData) => {
	projectIdOrName = parseProjectIdOrName(projectIdOrName);
	return await request('POST', `/projects/${ projectIdOrName }/members`, {
		user_id: userId,
		access_level: accessLevel,
		...additionalData,
	});
};

export default createProjectMember;
