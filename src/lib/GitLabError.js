import logger from './logger';

const log = logger.init('GitLabError.js');

/**
 * An error resulting from the GitLab API.
 * @access public
 * @augments Error
 * @author Martin Bories <martin.bories@megatherium.solutions>
 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
 * @example <caption>Throwing a GitLabError</caption>
 * import assert from 'assert';
 * import {
 * 	GitLabError,
 * } from '@megatherium/gitlab-api';
 *
 * const error = new GitLabError('GET', '/users', {}, {
 * 	error: {
 * 		parameters: 'Missing',
 * 		permission: [
 * 			'Access denied',
 * 		],
 * 	},
 * });
 * assert.throws(() => {
 * 	throw error;
 * }, error);
 * @example <caption>Throwing a GitLabError</caption>
 * import assert from 'assert';
 * import {
 * 	GitLabError,
 * } from '@megatherium/gitlab-api';
 *
 * const error = new GitLabError('GET', '/users', {}, {
 * 	error: {
 * 		parameters: 'Missing',
 * 	},
 * });
 * assert.throws(() => {
 * 	throw error;
 * }, error);
 * @license Megatherium Standard License 1.0
 * @module GitLabError
 */
class GitLabError extends Error {

	/**
	 * Contains a map of errors, corresponding to the parameter names that failed during a GitLab API request.
	 * @name GitLabError#error
	 */
	error = {}

	constructor(method, url, parameters, response) {
		log.error(`Error while requesting ${ method } ${ url }`, response);
		let message = response.error;
		if (!message && response.message) {
			if (typeof response.message === 'string') {
				message = response.message;
			} else {
				const keys = Object.keys(response.message);
				if (keys.length === 1) {
					message = response.message[keys[0]];
				} else {
					message = keys.map((key) => {
						return `${ key }=${ response.message[key] }`;
					}).join('\n');
				}
			}
		}

		super(message);

		this.error = response.error || response.message;
		if (typeof this.error === 'object') {
			for (const key in this.error) {
				if (Array.isArray(this.error[key])) {
					this.error[key] = this.error[key].join(',');
				}
			}
		}
	}

};

export default GitLabError;
