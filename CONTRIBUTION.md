# Contribution

If you want to contribute, create an own branch, for example:
- `git checkout -b feat-login`

If you are done, create a Pull Request.

## Commit messages

We follow the [angular commit message syntax](https://github.com/angular/angular.js/blob/master/DEVELOPERS.md#-git-commit-guidelines).
