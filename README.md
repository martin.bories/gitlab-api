# gitlab-api

See the whole [documentation](https://megatherium.gitlab.io/gitlab-api) or the [coverage report](https://megatherium.gitlab.io/gitlab-api/coverage).

## Getting started

Install the dependency:

`$ npm install @megatherium/gitlab-api`

Set up environment variables.

Start using it:

```
import gitlab from '@megatherium/gitlab-api';

(async() => {
	await gitlab.createProject('my-test-project');
})();
```

Configure an environment variable to give the module access to your GitLab API.

### Environment variables

 - `MEGATHERIUM_GITLAB_ACCESS_TOKEN` or `GITLAB_TOKEN` - the access token to login to GitLab instance
 - `MEGATHERIUM_GITLAB_BASE_URL` - the base url of the github repository; default: `https://gitlab.com/api/v4`


## API (22)

### Exports

- [createProject](module-createProject.html)`(pathOrName: string, additionalData?: object): Promise` Creates a new pipeline schedule for a project.
- [createProjectMember](module-createProjectMember.html)`(projectIdOrName: int | string, userId: int, accessLevel: int, additionalData?: object): Promise<object>` Adds a member to a project.
- [createProjectPipelineSchedule](module-createProjectPipelineSchedule.html)`(projectIdOrName: int | string, description: string, cron: string, additionalData?: object): Promise` Creates a new project.
- [createProjectVariable](module-createProjectVariable.html)`(projectIdOrName: int | string, variableData: object): Promise` Creates a new variable for a GitLab project.
- [createProjectVariable](module-createProjectVariable.html)`(projectIdOrName: int | string, variableData: object): Promise` Creates a new variable for a GitLab project.
- [createUserKey](module-createUserKey.html)`(parameters: object): Promise` Adds a new ssh key to the current user.
- [getProject](module-getProject.html)`(projectIdOrName: int | string, additionalParameters?: object): Promise` Retrieves the information about a project.
- [listProjects](module-listProjects.html)`(additionalParameters?: object): Promise<Array<object>>` Lists all projects from the current namespace, optionally filtered by parameters.
- [listProjectMembers](module-listProjectMembers.html)`(projectIdOrName: int | string, additionalParameters?: object): Promise<Array<object>>` Lists all the members of a project.
- [listProjectPipelineSchedules](module-listProjectPipelineSchedules.html)`(projectIdOrName: int | string, additionalParameters?: object): Promise<Array<object>>` Lists the pipeline schedules from a specific project.
- [listProjectVariables](module-listProjectVariables.html)`(projectIdOrName: int | string): Promise<Array<object>>` Lists the variables added to a project.
- [listUserKeys](module-listUserKeys.html)`(): Promise<Array<object>>` Lists the ssh keys from the current user.
- [listUsers](module-listUsers.html)`(additionalParameters?: object): Promise<Array<object>>` Lists the users. Can be filtered.
- [request](module-request.html)`(method: string, url: string, parameters?: object)` Performs an authenticated API request on the GitLab API.
- [removeProject](module-removeProject.html)`(pathIdOrName: string): Promise` Deletes a project permanently.
- [removeProjectMember](module-removeProjectMember.html)`(projectIdOrName: int | string, userId: int, additionalParameters?: object): Promise` Removes a member from a project.
- [removeProjectPipelineSchedule](module-removeProjectPipelineSchedule.html)`(projectIdOrName: int | string, pipelineScheduleId: int): Promise` Removes a pipeline schedule from a project.
- [removeProjectVariable](module-removeProjectVariable.html)`(projectIdOrName: int | string, variableName: string, additionalParameters?: object): Promise` Deletes a variable from a project.
- [removeUserKey](module-removeUserKey.html)`(int: keyId): Promise` Deletes a key of the current user permanently.
- [updateProjectMember](module-updateProjectMember.html)`(projectIdOrName: int | string, userId: int, accessLevel: int, additionalData?: object): Promise<object>` Updates the member of a project.
- [updateProjectPipelineSchedule](module-updateProjectPipelineSchedule.html)`(projectIdOrName: int | string, pipelineScheduleId: int, additionalData?: object): Promise` Updates a pipeline schedule of a project.
- [updateProjectVariable](module-updateProjectVariable.html)`(projectIdOrName: int | string, variableName: string, variableValue: string, additionalData?: object): Promise` Updates the data from a project variable.

### Scripts

The following scripts can be executed using `npm run`:
- `build` Builds the module.
- `build-docs` Builds the documentation.
- `build-source` Builds the source code.
- `build-tests` Builds test-cases from jsdoc examples.
- `clear` Clears the module from a previous build.
- `clear-coverage` Clears the coverage reports and caches.
- `clear-docs` Clears the previous documentation build.
- `clear-source` Clears the previous source build.
- `clear-tests` Clears the generated jsdoc example test files.
- `fix` Runs all automated fixes.
- `fix-lint` Automatically fixes linting problems.
- `release` Runs semantic release. Meant to be only executed by the CI, not by human users.
- `test` Executes all tests.
- `test-coverage` Generates coverage reports from the test results using [nyc](https://www.npmjs.com/package/nyc).
- `test-deps` Executes a [depcheck](https://www.npmjs.com/package/depcheck).
- `test-e2e` Executes End-to-End-Tests using [cucumber](https://github.com/cucumber/cucumber-js).
- `test-integration` Executes integration tests using [jest](https://jestjs.io/).
- `test-lint` Executes linting tests using [eslint](https://eslint.org/).
- `test-unit` Executes unit tests using [mocha](https://mochajs.org/).
- `update` Checks for dependency updates using [renovate](https://www.npmjs.com/package/renovate).
